package simplicity.controller;

import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.views.View;
import simplicity.domain.Greeting;

@Controller("/")
public class SimpleController {

    @View("index")
    @Get("/")
    public HttpStatus index() {
        return HttpStatus.OK;
    }

    @View("home")
    @Get("/views")
    public HttpResponse views() {
        return HttpResponse.ok(CollectionUtils.mapOf("loggedIn", true, "username", "Steve"));
    }

    @View("tables")
    @Get("/tables")
    public HttpStatus tables() {
        return HttpStatus.OK;
    }


    @View("result")
    @Post(value = "/greeting", consumes = MediaType.APPLICATION_FORM_URLENCODED)
    public HttpResponse greetingSubmit(HttpRequest<?> request) {
        String name = request.getParameters()
                .getFirst("Content")
                .orElse("Nobody");

        Greeting greet = new Greeting("1", name);
        return HttpResponse.ok(CollectionUtils.mapOf("greeting", greet));
    }


    @View("greeting")
    @Get("/greeting")
    public HttpResponse greeting() {
        return HttpResponse.ok(CollectionUtils.mapOf("greeting", new Greeting("10", "Steve")));
    }

}